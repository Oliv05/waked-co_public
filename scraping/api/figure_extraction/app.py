from PIL import Image
from matplotlib import pyplot as plt
import numpy as np
import json
import subprocess
import os
import shutil


def save_figure(img: np.array, x1: int, x2: float, y1: float, y2: float, filename: str) -> None:
    coordinates = (x1, y1, x2, y2)
    cropped = img.crop(coordinates)
    cropped.save(filename, "PNG")


OUT_DIR = "output_directory"
RESULTS_FILENAME = "deepfigures-results.json"
IMAGE_PATH = "{}-images/ghostscript/dpi200/{}.pdf-dpi200-page{}.png"
FIGURES_FOLDER = "extracted_figures"


def main(filename: str) -> None:
    # Handle filename and directories
    if " " in filename:
        raise FileNotFoundError("File name contains one or more whitespaces")
    full_filename = filename
    if not os.path.exists(full_filename):
        raise FileNotFoundError(f"No file named {full_filename}")
    if os.path.exists(OUT_DIR):
        shutil.rmtree(OUT_DIR)
    # Remove extension to get the prefix
    prefix = filename.replace(".pdf", "")

    #  Extract figures coordinates in OUT_DIR
    command = ["python3", "-m", "scripts.rundetection", OUT_DIR, full_filename]
    try:
        subprocess.check_call(command)
    except Exception as e:
        raise e

    subfolders = os.listdir(OUT_DIR)
    if len(subfolders) > 1:
        raise FileExistsError(f"There are unexpected files in {OUT_DIR}")
    subfolder = os.path.join(OUT_DIR, subfolders[0])
    # Extract figure coordinates from JSON file
    json_file = os.path.join(subfolder, prefix + RESULTS_FILENAME)
    with open(json_file, "rb") as f:
        file = f.read()
        formatted_json = json.loads(file)
        pages = {}
        for fig in formatted_json["figures"]:
            page = fig["page"]
            if page not in pages:
                pages[page] = []
            boundaries = fig["figure_boundary"]
            x1 = 2 * boundaries["x1"]
            x2 = 2 * boundaries["x2"]
            y1 = 2 * boundaries["y1"]
            y2 = 2 * boundaries["y2"]
            coordinates = (x1, x2, y1, y2)
            pages[page].append(coordinates)

    # Create directory for storing extracted figures
    os.mkdir(os.path.join(OUT_DIR, FIGURES_FOLDER))
    # Extract figures based on their coordinates
    for page_index, coordinates in pages.items():
        # The pages are indexed starting at 0
        page_index += 1
        print(f"Treating page {page_index}")
        # The page number is 4 characters long, with 0 padding on the left
        page_number = str(page_index).zfill(4)
        # Each page has its own png
        image_path = os.path.join(subfolder, IMAGE_PATH.format(full_filename, prefix, page_number))
        img = Image.open(image_path.format(page_number))
        for fig_num, coord in enumerate(coordinates):
            figure_name = f"page{page_number}_fig{fig_num + 1}.png"
            filepath = os.path.join(OUT_DIR, FIGURES_FOLDER, figure_name)
            save_figure(img, *coord, filepath)


main("test.pdf")

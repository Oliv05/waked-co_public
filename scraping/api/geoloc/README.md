# API Géolocalisation

## Exemples

### cURL

```
curl -i -H "Content-Type: application/json" -X POST -d '{"text":"France"}' http://localhost:5000/country_detection
```

```
curl -i -H "Content-Type: application/json" -X POST -d '{"country":"CN"}' http://localhost:5000/country_coordinates
```

```
curl -i -H "Content-Type: application/json" -X POST -d '{"country":"CN"}' http://localhost:5000/country_shape
```

### Python

```
python3 -c "import requests; import json; print(json.loads(requests.post('http://localhost:5001/country_coordinates', json = {'country':'FR'}).text));"
```

```
python3 -c "import requests; import json; c = json.loads(requests.post('http://api-geoloc:5000/country_detection', json = {'text':'france'}).text); print(c);"
```

## Docker

```
docker build -t api-geoloc . && docker run --rm -p 5000:5000 api-geoloc
```

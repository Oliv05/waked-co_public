import json
from geotext import GeoText
from flask import Flask, jsonify, request

app = Flask(__name__)

export_dict = {}
with open('country-codes.json') as json_data:
    data_dict = json.load(json_data)
    for i in data_dict:
        export_dict[i["fields"]["cou_iso2_code"]] = {}
        if "geo_point_2d" in i["fields"]:
            export_dict[i["fields"]["cou_iso2_code"]]["geo_point"] = i["fields"]["geo_point_2d"]
        if "geo_shape" in i["fields"]:
            export_dict[i["fields"]["cou_iso2_code"]]["geo_shape"] = i["fields"]["geo_shape"]

def detect_country(text):
    try:
        country = list(GeoText(text).country_mentions)[0]
    except:
        country = None
    return country

@app.route('/country_detection', methods=['POST'])
def country_detection():
  if not request.json or not 'text' in request.json:
    return "Bad query", 400
  return(
    jsonify(
      detect_country(
        request.json['text'])))
    
@app.route('/country_coordinates', methods=['POST'])
def country_coordinates():
  if not request.json or not 'country' in request.json:
    return "Bad query", 400
  if request.json['country'] in export_dict:
    if 'geo_point' in export_dict[request.json['country']]:
      coordinates = export_dict[request.json['country']]['geo_point']
    else:
      coordinates =  None 
  else:
      coordinates =  None  
  return(
    jsonify(coordinates))
    
@app.route('/country_shape', methods=['POST'])
def country_shape():
  if not request.json or not 'country' in request.json:
    return "Bad query", 400
  if request.json['country'] in export_dict:
    geo_shape = export_dict[request.json['country']]['geo_shape']
  else:
    geo_shape =  None  
  return(
    jsonify(geo_shape))

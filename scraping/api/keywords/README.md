# Keyword Extractor API

API d'extraction de mots clés à partir d'un texte.


  * Extraction des mots clés (keywords) par `yake`.
    * https://github.com/LIAAD/yake


  * Détection de la langue par `langdetect`.
    * https://pypi.org/project/langdetect/

## Exemples 

### Anglais

```
curl -i -H "Content-Type: application/json" -X POST -d '{"text":"We first described the 2019 novel coronavirus infection in 10 children occurring in areas other than Wuhan. The coronavirus diseases in children are usually mild and epidemiological exposure is a key clue to recognize pediatric case. Prolonged virus shedding is observed in respiratory tract and feces at the convalescent stage."}' http://localhost:5000/keywords
```

### Français

```
curl -i -H "Content-Type: application/json" -X POST -d '{"text":"Depuis l’émergence de cette nouvelle maladie, COVID-19, en janvier 2020, tous les agents de Santé publique France sont mobilisés et la complémentarité de leur métier s’exprime pleinement au service de la population. Leur action consiste à surveiller et comprendre la dynamique de cette épidémie, anticiper les différents scénarii et mettre en place des actions pour prévenir et limiter la transmission de ce virus sur le territoire national. La réserve sanitaire est fortement mobilisée depuis le début de l’épidémie.Les connaissances sur les caractéristiques du COVID-19 et de ce virus évoluant très rapidement jour après jour, la mobilisation est totale en coordination avec les instances françaises (Ministère des Solidarités et de la santé, Agences régionales de santé…) et internationales (Organisation mondiale de la santé, Centre européen de contrôle et de prévention des maladies).", "numOfKeywords":7, "max_ngram_size":1}' http://localhost:5000/keywords
```

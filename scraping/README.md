# Scrapers

| Éditeur  |  URL | Fréquence  |
|---|---|---|
| Wiley  | https://onlinelibrary.wiley.com/  | Continu  |
| bioRxiv  | https://www.biorxiv.org/ | Journalier  |
| PubMed  | https://www.ncbi.nlm.nih.gov/pubmed/ | Journalier  |
| MDPI  | https://www.mdpi.com/ | Journalier |
| ScienceDirect | https://www.sciencedirect.com/ |   |


## Docker

```
docker-compose build && docker-compose up
```


https://stackoverflow.com/questions/37458287/how-to-run-a-cron-job-inside-a-docker-container
import  os
import json
import urllib
import requests
from urllib.request import urlopen
from elasticsearch import Elasticsearch, helpers

print(str(datetime.today())[0:19]+", Scraper BioRxiv start.")

es = Elasticsearch(
        [os.environ['ELASTIC_URL']],
        http_auth=(os.environ['ELASTIC_LOGIN'], os.environ['ELASTIC_PASSWORD']), 
        scheme="https",
        port=443,
        timeout=1000000)
        
print(es.info())

url = "https://connect.biorxiv.org/relate/collection_json.php?grp=181"
json_url = urlopen(url)
data = json.loads(json_url.read())

print(len(data["rels"]), "Articles")

for i in data["rels"]:
    i['source'] = i.pop('rel_site')
    i['title'] = i.pop('rel_title')
    i["abstract"] = i.pop('rel_abs')
    i["doi"] = i.pop('rel_doi')
    i["authors"] = i.pop('rel_authors').split(";")
    i["publication_date"] = i.pop('rel_date')
    i["keywords"] = json.loads(
      requests.post('http://api-keywords:5000/keywords',
      json = {'text':i['abstract']}).text)
    try:
      #print(i)
      res = es.index(index="covid19_api_%s" %i["source"], id=i["doi"], body=i)
    except:
      print("error")
      
print(str(datetime.today())[0:19]+", Scraper BioRxiv stop.")

# docker build -t scraper-biorxiv biorxiv && docker run --rm --name scraper-biorxiv --network scraping_default scraper-biorxiv

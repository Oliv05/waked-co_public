import os
import json
import requests
from pymed import PubMed
from elasticsearch import Elasticsearch, helpers

print(str(datetime.today())[0:19]+", Scraper Pubmed start.")

es = Elasticsearch(
  [os.environ['ELASTIC_URL']],
  http_auth=(os.environ['ELASTIC_LOGIN'], os.environ['ELASTIC_PASSWORD']), 
  scheme="https", 
  port=443,
  timeout=1000000000)
                   
# Create a PubMed object that GraphQL can use to query
# Note that the parameters are not required but kindly requested by PubMed Central
# https://www.ncbi.nlm.nih.gov/pmc/tools/developers/
pubmed = PubMed(tool="Waked", email="my@email.address")

# Create a GraphQL query in plain text
query = """SARS-CoV-2 ("COVID-19"[All Fields] OR "COVID-2019"[All Fields] OR "severe acute respiratory syndrome coronavirus 2"[Supplementary Concept] OR "severe acute respiratory syndrome coronavirus 2"[All Fields] OR "2019-nCoV"[All Fields] OR "SARS-CoV-2"[All Fields] OR "2019nCoV"[All Fields] OR (("Wuhan"[All Fields] AND ("coronavirus"[MeSH Terms] OR "coronavirus"[All Fields])) AND (2019/12[PDAT] OR 2020[PDAT]))) OR SRAS[All Fields] OR COVID[All Fields] OR ("coronavirus infections"[MeSH Terms] OR ("coronavirus"[All Fields] AND "infections"[All Fields]) OR "coronavirus infections"[All Fields] OR "mers"[All Fields]) OR COV[All Fields] OR ("viruses"[MeSH Terms] OR "viruses"[All Fields] OR "virus"[All Fields])"""


# Execute the query against the API
results = pubmed.query(query, max_results = 100000000000)

# Loop over the retrieved articles
export_dicts = []
for article in results:
    # Print the type of object we've found (can be either PubMedBookArticle or PubMedArticle)
    #print(type(article))
    # Print a JSON representation of the object
    data = article.toJSON()
    data = json.loads(data)
    data["source"] = "pubmed"
    data["pubmed_id"] = data["pubmed_id"].split('\n')[0]
    #print(data['abstract'])
    try:
      data["keywords"] = json.loads(requests.post('http://api-keywords:5000/keywords', json = {'text':data['abstract']}).text)
    except Exception as e:
      #print(e)
      pass
    for index, i in enumerate(data["authors"]):
        data["authors"][index]["country"] = json.loads(requests.post('http://api-geoloc:5000/country_detection', json = {'text':data["authors"][index]["affiliation"]}).text)
        data["authors"][index]["geo_point_2d"] = json.loads(requests.post('http://api-geoloc:5000/country_coordinates', json = {'country':data["authors"][index]["country"]}).text)
    export_dicts.append(data)

actions = [
  {
    "_index": "covid19_api_pubmed_%s" %i["publication_date"][0:4],
    "_id": i["pubmed_id"],
    "_source": i
   }
  for i in export_dicts
]

helpers.bulk(es, actions)

print(str(datetime.today())[0:19]+", Scraper Pubmed stop.")

import os
import re
import json
import time
import yake
import random
import asyncio
import aiohttp
import requests
import pytesseract
import pandas as pd
from PIL import Image
from geotext import GeoText
from bs4 import BeautifulSoup
import dateutil.parser as parser
from datetime import datetime
from pdf2image import convert_from_path
from elasticsearch import Elasticsearch, helpers
from user_agent import generate_user_agent, generate_navigator

def get_authors(accordions):
  authors = []
  for accordion in accordions:
    author = {}
    name = accordion.find("span").text.split(" ")
    author['firstname'] = name[0]
    author['lastname'] = name[1]
    affiliations = accordion.find("div", {"class": "author-info"}).find_all("p")[0:-1]
    author['affiliations'] = [affiliation.text for affiliation in affiliations ]
    authors.append(author)

  return authors

def get_doi(txt):
  x = re.search("(https://doi.org/)(.*)", txt)
  doi = x[2]
  return doi

def unique(list_):   
  unique_list = [] 
  for x in list_: 
      if x not in unique_list: 
          unique_list.append(x) 
  return unique_list

def read_link(link,user_agent):
    link_raw = requests.get(link, headers={'User-Agent':user_agent})
    link_data = link_raw.content.decode()
    link_soup = BeautifulSoup(link_data, "html.parser")
    try:
        result = {}
        result['link'] = link
        result['title'] = link_soup.find("div", {"class": "citation"}).find('h1').text
        result['abstract'] = "".join([text.get_text()+" " for text in link_soup.find("section", {"class": "article-section__abstract"}).find_all("p")])
        result['keywords'] = json.loads(requests.post('http://api-keywords:5000/keywords', json = {'text':result['abstract']}).text)
        #get_keywords(result['abstract'])
        result['publication_date'] = link_soup.find("span", {"class": "epub-date"}).text
        result['authors'] = get_authors(link_soup.find_all("div", {"class": "accordion-tabbed__tab-mobile accordion__closed"}))

        result = detect_country(result)
        result['doi'] = get_doi(link_soup.find("a", {"class": "epub-doi"}).text)
        result['source'] = "wiley"
        result['publication_date'] = parser.parse(result['publication_date']).isoformat()[0:10]
        index_article(result)
    except:
        try:
            result = {}
            result['link'] = link
            
            result['title'] = link_soup.find("div", {"class": "citation"}).find('h1').text
            result['abstract'] = "".join([text.get_text()+" " for text in link_soup.find("section", {"class": "article-section__content"}).find_all("p")])
            result['keywords'] = json.loads(requests.post('http://api-keywords:5000/keywords', json = {'text':result['abstract']}).text)
            result['publication_date'] = link_soup.find("span", {"class": "epub-date"}).text
            result['authors'] = get_authors(link_soup.find_all("div", {"class": "accordion-tabbed__tab-mobile accordion__closed"}))
            result['doi'] = get_doi(link_soup.find("a", {"class": "epub-doi"}).text)
            result['source'] = "wiley"
            result = detect_country(result)
            result['publication_date'] = parser.parse(result['publication_date']).isoformat()[0:10]
            index_article(result)
        except:
            try:
                result = {}
                result['link'] = link
                result['title'] = link_soup.find("div", {"class": "citation"}).find('h1').text
                result['abstract'] = "".join([text.get_text()+" " for text in link_soup.find("section", {"class": "article-section article-section__full"}).find_all("p")])
                result['keywords'] = json.loads(requests.post('http://api-keywords:5000/keywords', json = {'text':result['abstract']}).text)
                result['publication_date'] = link_soup.find("span", {"class": "epub-date"}).text
                result['authors'] = get_authors(link_soup.find_all("div", {"class": "accordion-tabbed__tab-mobile accordion__closed"}))
                result = detect_country(result)
                result['doi'] = get_doi(link_soup.find("a", {"class": "epub-doi"}).text)
                result['source'] = "wiley"
                result['publication_date'] = parser.parse(result['publication_date']).isoformat()[0:10]
                index_article(result)
            except:
                try:
                    result = {}
                    result['link'] = link
                    result['title'] = link_soup.find("div", {"class": "citation"}).find('h1').text
                    result['abstract'] = "".join([text.get_text()+" " for text in link_soup.find("section", {"class": "article-section article-section__abstract"}).find_all("p")])
                    result['keywords'] = json.loads(requests.post('http://api-keywords:5000/keywords', json = {'text':result['abstract']}).text)
                    result['publication_date'] = link_soup.find("span", {"class": "epub-date"}).text
                    result['authors'] = get_authors(link_soup.find_all("div", {"class": "accordion-tabbed__tab-mobile accordion__closed"}))
                    result = detect_country(result)
                    result['doi'] = get_doi(link_soup.find("a", {"class": "epub-doi"}).text)
                    result['source'] = "wiley"
                    result['publication_date'] = parser.parse(result['publication_date']).isoformat()[0:10]
                    index_article(result)
                except:
                    try:
                        result = {}
                        result['link'] = link
                        result['title'] = link_soup.find("div", {"class": "citation"}).find('h1').text
                        result['publication_date'] = link_soup.find("span", {"class": "epub-date"}).text
                        result['publication_date'] = parser.parse(result['publication_date']).isoformat()[0:10]
                        result['authors'] = get_authors(link_soup.find_all("div", {"class": "accordion-tabbed__tab-mobile accordion__closed"}))
                        result = detect_country(result)
                        result['doi'] = get_doi(link_soup.find("a", {"class": "epub-doi"}).text)
                        result['source'] = "wiley"
                        time.sleep(random.randint(2,3))
                        img_url = "https://onlinelibrary.wiley.com"+link_soup.find("img", {"alt": "First page image"})['src']
                        r = requests.get(img_url, headers={'User-Agent':user_agent})
                        open('img.png', 'wb').write(r.content)
                        result['abstract'] = pytesseract.image_to_string(Image.open('img.png'))
                        os.remove('img.png')
                        result['keywords'] = json.loads(requests.post('http://api-keywords:5000/keywords', json = {'text':result['abstract']}).text)
                        index_article(result)
                    except Exception as e: 
                      print(e)
                      pass
        
def get_links(main_url,search_url,page,req,user_agent):
    url = search_url+req
    if(page > 1):
        url = url+"&startPage="+str(page)
    result = requests.get(url, headers= {'User-Agent':user_agent})
    data = result.content.decode()
    soup = BeautifulSoup(data, "html.parser")
    raw_links = soup.find_all(name='a',class_='publication_title')
    links = [main_url + i.get("href") for i in raw_links]
    return(links)

def get_research_terms():
    terms = ["SARS-CoV-2","COVID-19","COVID","Coronavirus","Virus"]
    countries = pd.read_csv("countries",sep=";").label

    research_terms = []#terms

    for term in terms:
        research_terms.append(term)
        for country in countries:
            research_terms.append(term+" "+country)
            
    random.shuffle(research_terms)
    
    return(research_terms)

def parse_query(
    query,
    main_url = "https://onlinelibrary.wiley.com",
    search_url = "https://onlinelibrary.wiley.com/action/doSearch?AllField=",
    max_pages = 99):
    user_agent = generate_user_agent()
    time.sleep(random.randint(2,3))
    for i in range(1,(max_pages+1)):
        time.sleep(random.randint(2,3))
        links = get_links(main_url,search_url,i,query,user_agent)
        if(len(links) == 0):
            return(0)
        for link in links:
            time.sleep(random.randint(2,3))
            read_link(link,user_agent)
        #print(str(datetime.today())[0:19]+", Wiley scraper, query \""+query+"\", page "+str(i)+" scraped, "+str(get_articles_count("covid19_api_wiley*"))," articles.")
        #print_articles_count("covid19_api_wiley*")
        #print("Query \""+query+"\", page "+str(i)+" scraped, "+str(len(articles))," articles, "+str(len(fail))," fails.")

def parse_all():
    queries = get_research_terms()
    for query in queries:
        try:
            parse_query(query)
        except:
            time.sleep(10)

def index_article(article):
    es = Elasticsearch(
        [os.environ['ELASTIC_URL']],
        http_auth=(os.environ['ELASTIC_LOGIN'], os.environ['ELASTIC_PASSWORD']), 
        scheme="https", 
        port=443,
        timeout=1000000)
    index_name = "covid19_api_"+article["source"]+"_"+article["publication_date"][0:4]
    es.indices.create(
        index_name,
        ignore=400)
    #print(article)
    #print(article['publication_date'])
    es.index(
        index=index_name,
        id=article["doi"],
        body=article)

def detect_country(article):
  for index, i in enumerate(article["authors"]):
    affiliations = " ".join(article["authors"][index]["affiliations"]).strip()
    article["authors"][index]["country"] =  json.loads(
      requests.post('http://api-geoloc:5000/country_detection',
      json = {'text':affiliations}).text)
    article["authors"][index]["geo_point_2d"] = json.loads(requests.post(
      'http://api-geoloc:5000/country_coordinates',
      json = {'country':article["authors"][index]["country"] }).text)
  return(article)

def get_articles_count(index):
    es = Elasticsearch(
        [os.environ['ELASTIC_URL']],
        http_auth=(os.environ['ELASTIC_LOGIN'], os.environ['ELASTIC_PASSWORD']), 
        scheme="https", 
        port=443,
        timeout=1000000)
    return(es.cat.count(index).split()[2])

def main():
  print(str(datetime.today())[0:19]+", Scraper Wiley start.")
  parse_all()
  print(str(datetime.today())[0:19]+", Scraper Wiley stop.")

if __name__ == "__main__":
    main()

